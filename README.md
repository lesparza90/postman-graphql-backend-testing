# BackEnd Challenge - Jose Luis Esparza Argumaniz #


### How to run the tests locally using Newman ###

* npm install -g newman
* newman run <collection> -e <environment>
* newman run JoseLuisEsparzaArgumaniz_GraphqlWorkshop.postman_collection.json -e QA.postman_environment.json

### How to run Newman using Docker ###

* docker run -v <collection and environment directory>:/etc/newman -t postman/newman_alpine33 <collection> --environment="<environment>
* docker run -v $(pwd):/etc/newman -t postman/newman_alpine33 run JoseLuisEsparzaArgumaniz_GraphqlWorkshop.postman_collection.json --environment="QA.postman_environment.json"

### look at the pipeline section ###

* in the pipeline section you will find a docker image running the Integration Test with newman


![Integration Test in a Bitbucket pipeline](pipeline.png)
